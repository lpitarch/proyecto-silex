<?php 
require_once __DIR__.'/vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;

// código de los controladores
$app->get('/hola/{name}', function($name) use($app) { 
    return '<h1>hola '.$app->escape($name)."</h1>"; 
}); 

$app->get('/hola', function() { 
return '<h1>Hola desde silex</h1> '; 
}); 

$app->run();

 ?>